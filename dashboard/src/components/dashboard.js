import { Button1 } from "./styled/button1";
import { Link } from "react-router-dom";
const Dashboard = () => {
  return (
    <div>
      <p>Dashboard</p>
      <Link to="/home" style={{textDecoration: "none"}}>
        <Button1 type="button">HOME</Button1>
      </Link>
    </div>
  );
};

export default Dashboard;
