import styled from "styled-components";

export const Button1 = styled.button`
  display: inline-block;
  color: palevioletred;
  cursor: pointer;
  font-size: 1em;
  padding: 0.25em 1em;
  margin: 20px 0px;
  border: 2px solid palevioletred;
  border-radius: 3px;
  display: block;
  width: 200px;
`;