import { Button1 } from "./styled/button1";
import { Link } from "react-router-dom";
const Home = () => {
  return (
    <div>
      <Link to="/l" style={{ textDecoration: "none" }}>
        <Button1>LOG IN</Button1>
      </Link>
      <Link to="/home" style={{ textDecoration: "none" }}>
        <Button1>HOME</Button1>
      </Link>
    </div>
  );
};

export default Home;
