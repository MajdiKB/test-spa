import { registerApplication, start } from "single-spa";

registerApplication({
  name: "@sd/home",
  app: () => System.import("@sd/home"),
  activeWhen: ["/home"],
});

registerApplication({
  name: "@sd/login",
  app: () => System.import("@sd/login"),
  activeWhen: ["/l"],
});

registerApplication({
  name: "@sd/dashboard",
  app: () => System.import("@sd/dashboard"),
  activeWhen: ["/dashboard"],
});

start({
  urlRerouteOnly: true,
});
