import { Link } from "react-router-dom";
import { Button1 } from "./styled/button1";
const Logout = () => {
  return (
    <Link to="/l/" style={{ textDecoration: "none" }}>
      <Button1 type="button">BACK</Button1>
    </Link>
  );
};
export default Logout;
