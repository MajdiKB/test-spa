import { useForm } from "react-hook-form";
import { Button1 } from "./styled/button1";
import { Link, useNavigate } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    console.log(data);
    navigate("/dashboard");
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input placeholder="Insert email" {...register("name")} />
      <input {...register("password", { required: true })} />

      <Button1 type="submit">LOG IN</Button1>

      <Link to="/home" style={{ textDecoration: "none" }}>
        <Button1 type="button">HOME</Button1>
      </Link>
      <Link to="/l/logout" style={{ textDecoration: "none" }}>
        <Button1 type="button">LOG OUT</Button1>
      </Link>
      {errors.password && <span>Password is required</span>}
    </form>
  );
};

export default Login;
