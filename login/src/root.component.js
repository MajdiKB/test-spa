import Login from "./components/login";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Logout from "./components/logout";
const Root = (props) => {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<Login />} />
        <Route path="/l" element={<Login />} />
        <Route path="/l/logout" element={<Logout />} />
        <Route path="*" element={<Login />} />
      </Routes>
    </BrowserRouter>
  );
};
export default Root;
